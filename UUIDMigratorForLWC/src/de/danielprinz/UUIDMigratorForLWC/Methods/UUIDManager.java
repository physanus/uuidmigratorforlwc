package de.danielprinz.UUIDMigratorForLWC.Methods;

import de.danielprinz.UUIDMigratorForLWC.Main;
import de.danielprinz.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.UUID;

/**
 * Created by daniela on 01.11.2016.
 */
public class UUIDManager {

    public static HashMap<String, UUID> players = new HashMap<String, UUID>() {{
        put("KevinTheGod99", UUID.fromString("fcc59284-7653-419d-8ec7-512e93570d7e"));
        put("HenneThePine", UUID.fromString("bb54c531-79dd-49df-aa2d-db3900b7b093"));
        put("salkin31", UUID.fromString("33c5016c-9e6c-41a8-a885-9e9b662aae1b"));
        put("benj1111111", UUID.fromString("7739f3f5-49bf-4856-b71d-4eb7798377bd"));
        put("curry11", UUID.fromString("2d0e165c-473c-48bf-b0c0-0e2567a70535"));
        put("KianaAhris", UUID.fromString("999e135e-bfbf-47e3-a0d3-0e5bc9f49045"));
        put("Play_Time_TV", UUID.fromString("d5e45057-b6f7-4ef2-b65f-dfc112541774"));
        put("xSourcraftHD", UUID.fromString("6cfd44ba-8ea4-4cc5-8bae-9946a68fafb7"));
        put("BlvckCraft", UUID.fromString("24780827-cd6b-41fe-b78a-a517167d9098"));
        put("FCB2002", UUID.fromString("a7e0fb3c-9cb5-40b0-9b28-406ae32aaccb"));
        put("Hubel", UUID.fromString("211c1bbc-be20-44f9-b900-13eba63779d8"));
        put("WuselineWusel", UUID.fromString("999e135e-bfbf-47e3-a0d3-0e5bc9f49045"));
        put("LolPopGirl", UUID.fromString("07e10d22-811a-42cf-a60d-73938aa89c8e"));
        put("xAspireYT", UUID.fromString("87f6b610-b1b6-4d99-8d8b-a1da6db9480d"));
        put("SuperMitteGirl", UUID.fromString("07e10d22-811a-42cf-a60d-73938aa89c8e"));
        put("Marlon2882003", UUID.fromString("92458d61-3fcd-44a1-b5b5-eefd2762eaa1"));
        put("DeathgodThanatos", UUID.fromString("f55ea72b-caf5-4c3a-9ba3-701cf555baa7"));
        put("NicolSpielt", UUID.fromString("da4c613c-5fe0-4d58-8f5a-68798657cb5d"));
        put("StefensLP", UUID.fromString("b194bee5-b9d1-44c8-a1b9-5c95abbe59d3"));
        put("BananaxLord", UUID.fromString("c3812b1e-d6ee-46d6-b035-25cc8bb149e8"));
        put("MoeLP02", UUID.fromString("dce3f2f9-027e-4795-ae5a-e17709db73fa"));
        put("Nova_445", UUID.fromString("14da0b51-2813-4676-95b3-12e82301aadd"));
        put("Boixioh_HD", UUID.fromString("226230df-9fc6-4cb0-9a55-7a79ca512bcd"));
        put("sunshine2004", UUID.fromString("7f46f60f-2658-4f44-b8d7-053da7aeb6c9"));
        put("BUDDAYT", UUID.fromString("82c754b4-59b2-42bc-8c5c-779014bfe61b"));
        put("Cronoth", UUID.fromString("825151be-91f3-437a-bbfb-cf1e5f58ae07"));
        put("_Bizyyy", UUID.fromString("4867499e-8943-4bf4-b9e4-0663c74c4df9"));
        put("Th3Seba", UUID.fromString("b8ea6e4a-84e0-4362-b4e2-fe4f7c19e292"));
        put("KirikoKen", UUID.fromString("c3c3a7ab-edef-4c29-b5c0-c1e7310077b3"));
        put("UnLeqitKiller", UUID.fromString("9ff5587c-48db-4b6e-8254-55c9aff51d7a"));
        put("BronziiJr", UUID.fromString("3472b9a0-8e81-4e9c-b5ac-4ce21e23db95"));
        put("BlockZuDemHit", UUID.fromString("41a59be9-9674-4293-96f2-1791a14f2f69"));
        put("Inferno_LP", UUID.fromString("970a6206-32f5-4c93-a55f-72900bba289e"));
        put("UnLogischerBlade", UUID.fromString("82c754b4-59b2-42bc-8c5c-779014bfe61b"));
        put("_Florian_", UUID.fromString("df954637-0aa8-434e-a729-cd8d5ed419b3"));
        put("Itz_NeroGraffity", UUID.fromString("56f740b2-6336-480b-9354-4ab5a36d11a8"));
        put("_xXSHEEPXx_", UUID.fromString("091d65ed-351c-4e65-ba79-85f042e7642e"));
        put("hidanax", UUID.fromString("a0781687-e4e5-44be-be1f-1070492d5caa"));
        put("jedi_marvin", UUID.fromString("b3684126-4b2d-4864-856c-08bd2a7210ac"));
        put("HackerBoyLP", UUID.fromString("a548b53c-40ec-48ff-b9f7-cf17b7146dc8"));
        put("SpLasH_xBeast", UUID.fromString("b5713de3-bdcb-4618-979f-c8cd278c28c7"));
        put("BrotEagle", UUID.fromString("dea0f942-70cb-4d0e-92fa-a4a5422284fa"));
        put("EingekacktHD", UUID.fromString("bdd7136c-3bbc-41f7-9702-6c012c668804"));
        put("DONI4k", UUID.fromString("fa3d42a0-8bea-4a29-bfa9-789d1b5d9df6"));
        put("Relox_YT", UUID.fromString("a9dd7058-513d-4592-b98f-2824676e1ae9"));
        put("XxGalaxie_PvPxX", UUID.fromString("a04eb842-fdb0-4319-9660-32758d71cc28"));
        put("NooowYork", UUID.fromString("0dc41839-40dd-4da2-8435-ae8e5b1ec599"));
        put("Wendl_ll", UUID.fromString("e63764e8-cdd4-40a8-adbd-6f63acefd88a"));
        put("ExBunnyFisher", UUID.fromString("821caa4a-ed97-43ed-8a94-1c58f0d61dd2"));
        put("UmgekehrtYT", UUID.fromString("9a51e61e-8462-419d-8e28-69045720ac10"));
        put("Fox_Craft", UUID.fromString("c3812b1e-d6ee-46d6-b035-25cc8bb149e8"));
        put("Thorsten110", UUID.fromString("163d430a-3a6f-4511-9224-958a95fc0f2f"));
        put("Voyage_HD", UUID.fromString("3f6ec7e9-1437-4085-bfb2-b9d0a573c8d0"));
        put("NolifeNolife", UUID.fromString("ef3739cf-543c-4def-bac8-1467f39d3a41"));
        put("xXZolenLPXx", UUID.fromString("539b6a16-1c69-4e2d-b14f-53973f4bb0ac"));
        put("Goldlight_12", UUID.fromString("4588dd36-252a-44ab-8c2e-74f4f09f43fd"));
        put("ManuelKinglp", UUID.fromString("5861996c-aee2-460f-b288-9c3b85d52ee2"));
        put("Implay_Nick", UUID.fromString("67c5d2e4-1553-4a67-82d6-daf4ca4d1be1"));
        put("theyHD", UUID.fromString("51fd9234-e084-4ca9-81cb-0cd5441eba48"));
        put("EnderLordzZ", UUID.fromString("d8112171-2529-4c07-9f8f-c994f892302f"));
        put("TypiGirly005_", UUID.fromString("d8e6338d-47ba-4d3d-a5ff-6b5d369c41d4"));
        put("BlvckVillain", UUID.fromString("9a51e61e-8462-419d-8e28-69045720ac10"));
        put("YolodogLP", UUID.fromString("60519ee8-21df-49cd-997b-2ae312004866"));
        put("Skyst0rmer", UUID.fromString("4c9bb33b-50ea-4a32-afe9-c8618b727b20"));
        put("CarneCrafter", UUID.fromString("f181c93b-858b-4313-a41d-98b8828b5e43"));
        put("Kuchenfurz", UUID.fromString("95b564df-0f07-4ced-91c6-83ab670d640d"));
        put("BestronYT", UUID.fromString("f161870b-5a30-4121-a782-d7a56b191468"));
        put("Snipeeeee", UUID.fromString("1d549661-1424-4f37-897f-dfc618dbf71c"));
        put("DeDohrr", UUID.fromString("95cf5ff7-338a-44f5-89bb-eb4b08ad615f"));
        put("LonelyWish", UUID.fromString("be36602b-ab18-4ad5-b808-75951cf53f99"));
        put("PvPskillerxDHDLP", UUID.fromString("4a803e31-8f36-4d7a-a348-a479d4c88575"));
        put("Im_SkillZz", UUID.fromString("bad4e5c3-d674-4fca-b183-b4a7fc839b4a"));
        put("G0dLikeQQ", UUID.fromString("da3b8b65-8188-4033-b215-7aa09d2014b3"));
        put("AllesGut", UUID.fromString("cb10d896-36d6-40f4-8021-d41834e38998"));
        put("Eazy_G", UUID.fromString("65932b70-7681-4bdd-ae96-dc18dfb2b8ec"));
        put("Megaball111", UUID.fromString("8e4c913f-72fc-4d32-96c5-4d0b0fc38508"));
        put("PhilTheCyclops", UUID.fromString("88ae5b9d-f8d8-4449-96ae-6464dc316d27"));
        put("Eberhard_Framens", UUID.fromString("f55ea72b-caf5-4c3a-9ba3-701cf555baa7"));
        put("JamieLeeAngel", UUID.fromString("95467e88-e4b4-488b-9e97-0e2d42a42f21"));
        put("ToniMusic", UUID.fromString("517414cf-2bea-4d7a-addf-7b062e5f254b"));
        put("byJaaan", UUID.fromString("ceff73d6-80ee-4759-856d-a40939207267"));
        put("SpieleGleich", UUID.fromString("7bd2db43-63e7-4bdb-b25b-8328626a8dae"));
        put("SpieleNicht", UUID.fromString("a456fca8-4781-4ae3-986f-6c7a834c03cc"));
        put("Schreini", UUID.fromString("02036cf1-d7a9-471c-b247-4efc11499338"));
        put("Davidmaster80", UUID.fromString("12f4b0ec-470c-4a01-b6bc-9f02c1b2f5eb"));
        put("1337iH4X0rZ", UUID.fromString("f9c20691-4770-4056-88a3-5dcd4e6993b6"));
        put("AggroVau", UUID.fromString("8ad3aaf1-97f9-4fee-b6ee-a64a57e9eaf0"));
        put("Schinken64", UUID.fromString("18db192a-4888-4dd5-b087-78bd14d784dc"));
        put("thebigkevin", UUID.fromString("1f5c2e3c-74f9-4287-b3a7-33c463b43d72"));
        put("PassiV_", UUID.fromString("1ea2d2da-cd0a-41f9-b2ee-ec98738a94a5"));
        put("8BitLP", UUID.fromString("f6f0c1a7-d645-4558-ac45-915d09565acb"));
        put("xXThobiXx", UUID.fromString("f33a2f2c-e8af-495b-a236-c7c7e310230f"));
        put("MrBlackWolfYT", UUID.fromString("38d5a1c7-e44e-4620-9177-73969a6c0f3e"));
        put("NeBeL_CoRe", UUID.fromString("e761040c-ba81-4c1d-aac0-eeb1d970ad4a"));
        put("RealFlarez", UUID.fromString("4a803e31-8f36-4d7a-a348-a479d4c88575"));
        put("Jokerplays_HD", UUID.fromString("2775668e-1731-4cf9-b76d-228bd28a4318"));
        put("KnuddelMeinTeddy", UUID.fromString("ceff73d6-80ee-4759-856d-a40939207267"));
        put("xXKu3rbislordXx", UUID.fromString("fb9fe51d-3c92-479d-ac54-9d6ebecc6f68"));
        put("Leschie", UUID.fromString("f68e4b63-2261-4556-8136-1be7b945da4b"));

        //put("", UUID.fromString(""));
    }};

    public static UUID fetch(String ownerOld) {

        if(!players.containsKey(ownerOld)) {
            try {
                // JSONObject json = new JSONObject(new Scanner(new URL("https://mcapi.ca/uuid/player/" + ownerOld).openStream(), "UTF-8").useDelimiter("\\A").next().replaceAll("\\[", "").replaceAll("\\]", ""));
                //String a = new Scanner(new URL("https://api.mojang.com/users/profiles/minecraft/" + ownerOld).openStream(), "UTF-8").useDelimiter("\\A").next().replaceAll("\\[", "").replaceAll("\\]", "");
                JSONObject json = new JSONObject(new Scanner(new URL("https://api.mojang.com/users/profiles/minecraft/" + ownerOld).openStream(), "UTF-8").useDelimiter("\\A").next().replaceAll("\\[", "").replaceAll("\\]", ""));

                String uuid_string = String.valueOf(json.get("id"));
                uuid_string = uuid_string.substring(0, 8) + "-" + uuid_string.substring(8, 12) + "-" + uuid_string.substring(12, 16) + "-" + uuid_string.substring(16, 20) + "-" + uuid_string.substring(20);
                UUID uuid = UUID.fromString(uuid_string);

                players.put(ownerOld, uuid);
                return uuid;
            } catch (IOException e) {
                e.printStackTrace();
            //} catch (IllegalArgumentException e) { // old API
            } catch (NoSuchElementException e) {
                // the username does not exist anymore
                players.put(ownerOld, null);
                Main.error("UUID for " + ownerOld + " not found. Please change them manually.");
                return null;
            }
            players.put(ownerOld, null);
            Main.error("UUID for " + ownerOld + " not found. Please change them manually.");
            return null; // unkown error, should not happen
        }
        return players.get(ownerOld);

    }

    public static boolean isValid(UUID uuid) {
        return uuid != null;
    }

}
