package de.danielprinz.UUIDMigratorForLWC.Methods;

import de.danielprinz.UUIDMigratorForLWC.Main;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by daniela on 01.11.2016.
 */
public class DBManager {

    private final Main plugin;
    private final String filename;
    public DBManager(Main plugin, String filename) {
        this.plugin = plugin;
        this.filename = filename;
    }

    private HashMap<Integer, UUID> updateProtections;
    private HashMap<Integer, String> oldProtections;

    public void analyze() {

        /*
         * Get columns to be changed in lwc_protections
         */

        String table = "LWC_PROTECTIONS";
        String columnname = "OWNER";

        this.plugin.log("========================  Fetching usernames | " + table + " ========================");
        convertOwner(table, columnname);
        this.plugin.log("done.");
        this.plugin.log("");


        this.plugin.log("========================  UUID Conversion List | " + table + "  ========================");
        for(Map.Entry<String, UUID> player : UUIDManager.players.entrySet()) {
            if(UUIDManager.isValid(player.getValue())) {
                this.plugin.log(player.getKey() + " | " + player.getValue() + " | " + player.getValue().toString().replaceAll("-", ""));
            }
        }
        this.plugin.log("done.");
        this.plugin.log("");


        /*
         * Update lwc_protections
         */

        this.plugin.log("=======================  Updating Table | " + table + " =======================");

        try {
            Connection connection = this.plugin.getSQLConnection(this.filename);

            for(Map.Entry<Integer, UUID> entry : this.updateProtections.entrySet()) {
                int id = entry.getKey();

                Statement stmt = connection.createStatement();
                String sql = "UPDATE " + table + " set OWNER = \"" + this.updateProtections.get(id) + "\" where ID=" + id + ";";
                stmt.executeUpdate(sql);
                connection.commit();
                stmt.close();

                this.plugin.log("Column #" + id + ": " + this.oldProtections.get(id) + " ==> " + this.updateProtections.get(id));
            }
            connection.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.plugin.log("done.");
        this.plugin.log("");







        /*
         * Get columns to be changed in lwc_history
         */

        table = "LWC_History";
        columnname = "PLAYER";

        this.plugin.log("========================  Fetching usernames | " + table + " ========================");
        convertOwner(table, columnname);
        this.plugin.log("done.");
        this.plugin.log("");


        this.plugin.log("========================  UUID Conversion List | " + table + "  ========================");
        for(Map.Entry<String, UUID> player : UUIDManager.players.entrySet()) {
            if(UUIDManager.isValid(player.getValue())) {
                this.plugin.log(player.getKey() + " | " + player.getValue() + " | " + player.getValue().toString().replaceAll("-", ""));
            }
        }
        this.plugin.log("done.");
        this.plugin.log("");


        /*
         * Update lwc_history
         */

        this.plugin.log("=======================  Updating Table | " + table + " =======================");

        try {
            Connection connection = this.plugin.getSQLConnection(this.filename);

            for(Map.Entry<Integer, UUID> entry : this.updateProtections.entrySet()) {
                int id = entry.getKey();

                Statement stmt = connection.createStatement();
                String sql = "UPDATE " + table + " set PLAYER = \"" + this.updateProtections.get(id) + "\" where ID=" + id + ";";
                stmt.executeUpdate(sql);
                connection.commit();
                stmt.close();

                this.plugin.log("Column #" + id + ": " + this.oldProtections.get(id) + " ==> " + this.updateProtections.get(id));
            }
            connection.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.plugin.log("done.");
        this.plugin.log("");

    }











    private void convertOwner(String table, String columnname) {

        this.updateProtections = new HashMap<Integer, UUID>();
        this.oldProtections = new HashMap<Integer, String>();

        try {

            Connection connection = this.plugin.getSQLConnection(this.filename);
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + table + ";");

            while (rs.next()) { // for every row

                int id = rs.getInt("id");
                String ownerOld = rs.getString(columnname);

                if (!ownerOld.matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}")) {
                    // ownerOld is no uuid
                    UUID uuid = UUIDManager.fetch(ownerOld);
                    if(UUIDManager.isValid(uuid)) {
                        updateProtections.put(id, uuid);
                        oldProtections.put(id, ownerOld);
                    }
                }
            }
            rs.close();
            stmt.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
