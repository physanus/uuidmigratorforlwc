package de.danielprinz.UUIDMigratorForLWC;

import de.danielprinz.UUIDMigratorForLWC.Methods.DBManager;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by daniela on 01.11.2016.
 */
public class Main {

    private String curfullpath = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    private String curpath = this.curfullpath.substring(1, this.curfullpath.lastIndexOf("/")) + "/";
    private String curfile = this.curfullpath.substring(this.curfullpath.lastIndexOf("/")+1);

    public static void main(String[] args) {
        new Main().run("lwc.db");
    }

    private void run(String filename) {

        File file = new File(filename);

        if(!file.exists()) {
            this.error("ERROR: The file does not exist: " + this.curpath + filename);
            return;
        }

        /*
         SQLite
         */

        DBManager dbManager = new DBManager(this, filename);
        dbManager.analyze();


    }

    public Connection getSQLConnection(String filename) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection c = DriverManager.getConnection("jdbc:sqlite:" + filename);
        c.setAutoCommit(false);
        return c;
    }

    public static void log(String s) {
        System.out.println(s);
    }
    public static void error(String s) {
        System.err.println(s);
    }

}
